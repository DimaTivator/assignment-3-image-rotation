#include <./image.h>

enum rotation_result {
    ROTATION_OK,
//    ROTATION_ERROR
};

enum rotation_result rotate(struct image* img, int64_t angle);
