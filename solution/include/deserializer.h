#ifndef DESERIALIZER_H
#define DESERIALIZER_H

#include <stdio.h>

struct image;

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS_NUMBER,
    READ_INVALID_HEADER,
    MEMORY_ALLOCATION_ERROR
};

extern const char* error_messages[];

enum read_status from_bmp(FILE* in, struct image* img);

#endif
