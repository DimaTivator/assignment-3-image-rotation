#include <stdio.h>

struct image;

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, struct image const* img);
