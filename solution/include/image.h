#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel* data;
};

enum status {
    OK,
    FAIL
};

struct maybe_image {
    struct image img;
    enum status status;
};

struct maybe_image create_image(uint64_t w, uint64_t h);

void free_image(struct image* img);

