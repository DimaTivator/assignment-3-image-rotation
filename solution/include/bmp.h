#include <./deserializer.h>
#include <./image.h>
#include <./serializer.h>
#include <./utils.h>

#define HEADER_SIZE 40
#define MB 0x4D42

struct __attribute__((packed)) bmp_header {

    // indicates file-type bmp
    uint16_t bfType;

    // file size in bytes
    uint32_t  bfileSize;

    // reserved for possible future use
    uint32_t bfReserved;

    // the offset, in bytes, from the beginning of the file to the start of the image data
    uint32_t bOffBits;

    // the size of the structure
    uint32_t biSize;

    // the width of the image in pixels
    uint32_t biWidth;

    // the height of the image in pixels
    uint32_t  biHeight;

    // the number of color plates (typically set to 1)
    uint16_t  biPlanes;

    // the number of bits per pixel
    uint16_t biBitCount;

    // type of compression
    uint32_t biCompression;

    // the size of the raw bitmap data in bytes (including padding)
    uint32_t biSizeImage;

    // the horizontal resolution of the target device (output device) for the bitmap
    uint32_t biXPelsPerMeter;

    // the vertical resolution of the target device (output device) for the bitmap
    uint32_t biYPelsPerMeter;

    // the number of colors used in the bitmap
    uint32_t biClrUsed;

    // the number of important colors
    uint32_t  biClrImportant;
};

