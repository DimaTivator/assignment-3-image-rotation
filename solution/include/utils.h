#include <stdint.h>

/*
 * If the width of the image in bytes is a multiple of four,
 * then the lines go one after the other without spaces.
 * In the other case, it is padded with garbage bytes to the nearest multiple of four.
 */
uint64_t calc_padding(uint64_t w);
