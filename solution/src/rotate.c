#include <../include/rotate.h>

static void transpose(struct image* img) {

    struct pixel* transposed_image = malloc(img->width * img->height * sizeof (struct pixel));

    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width; j++) {
            transposed_image[j * img->height + i] = img->data[i * img->width + j];
        }
    }

    free_image(img);
    img->data = transposed_image;

    uint64_t t = img->width;
    img->width = img->height;
    img->height = t;
}

static void reverse_rows(struct image* img) {
    for (uint64_t i = 0; i < img->height; i++) {
        for (uint64_t j = 0; j < img->width / 2; j++) {
            struct pixel tmp = img->data[i * img->width + j];
            img->data[i * img->width + j] = img->data[i * img->width + (img->width - j - 1)];
            img->data[i * img->width + (img->width - j - 1)] = tmp;
        }
    }
}

static void rotate_by_90(struct image* img) {
    transpose(img);
    reverse_rows(img);
}

enum rotation_result rotate(struct image* img, int64_t angle) {

    angle = (-angle + 360) % 360;
    for (int64_t i = 0; i * 90 < angle; i++) {
        rotate_by_90(img);
    }

    return ROTATION_OK;
}

