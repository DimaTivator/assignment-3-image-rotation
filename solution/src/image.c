#include <../include/image.h>

struct maybe_image create_image(uint64_t w, uint64_t h) {

    struct maybe_image result;
    struct image img;

    if (w <= 0 || h <= 0) {
        result.status = FAIL;
        return result;
    }

    img.data = (struct pixel*)calloc(sizeof(struct pixel), w * h);

    // checking if memory hasn't been allocated
    if (img.data == NULL) {
        result.status = FAIL;
        return result;
    }

    img.width = w;
    img.height = h;

    result.status = OK;
    result.img = img;

    return result;
}

void free_image(struct image* img) {
    // if img->data == NULL `free` function will do nothing
    free(img->data);
    img->data = NULL;
}
