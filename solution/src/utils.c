#include <../include/utils.h>

inline uint64_t calc_padding(uint64_t w) {
    // 1 pixel -- 3 bytes
    uint64_t bytes = w * 3;
    return (4 - bytes % 4) % 4;
}
