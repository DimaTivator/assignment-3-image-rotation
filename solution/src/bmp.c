#include <../include/bmp.h>


enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    // reading bmp header
    fread(&header, sizeof(struct bmp_header), 1, in);

    // checking header's size
    if (header.biSize != HEADER_SIZE) {
        return READ_INVALID_HEADER;
    }

    // checking signature
    if (header.bfType != MB) {
        return READ_INVALID_SIGNATURE;
    }

    // we support only 24 bits per pixel
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS_NUMBER;
    }

    struct maybe_image m_image = create_image(header.biWidth, header.biHeight);

    if (m_image.status == FAIL) {
        return MEMORY_ALLOCATION_ERROR;
    }

    *img = m_image.img;

    // skip the space between image and header (if it is)
    size_t skip = fseek(in, (long) header.bOffBits, SEEK_SET);

    // fseek returns 0 if it successfully moves the pointer
    if (skip) {
        free_image(img);
        return READ_INVALID_BITS_NUMBER;
    }

    size_t padding = calc_padding(img->width);

    // reading the image pixels line by line in order to skip padding
    for (size_t i = 0; i < img->height; i++) {

        size_t read = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);

        // fread returns the number of successfully read items
        if (read != img->width) {
            free_image(img);
            return READ_INVALID_BITS_NUMBER;
        }

        // shifting the pointer by padding size
        int shift = fseek(in, (long) padding, SEEK_CUR);

        // fseek returns 0 if it successfully moves the pointer
        if (shift) {
            free_image(img);
            return READ_INVALID_BITS_NUMBER;
        }
    }

    return READ_OK;
}


struct bmp_header get_header(struct image const* img) {
    size_t padding = calc_padding(img->width);
    size_t img_size = (img->width + padding) * img->height;

    struct bmp_header header = {
            .bfType = MB,
            .bfileSize = sizeof(struct bmp_header) + img_size * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    return header;
}


enum write_status to_bmp(FILE* out, struct image const* img) {

    size_t padding = calc_padding(img->width);
    struct bmp_header header = get_header(img);

    // writing header
    size_t write_header = fwrite(&header, sizeof(struct bmp_header), 1, out);

    // fwrite returns the number of successfully written items
    if (write_header != 1) {
        return WRITE_ERROR;
    }

    // we use uint32_t here because it's size is 4 bytes and the maximum padding we need is 3 bytes
    // (we only need a temporary buffer to take 0-bytes from it)
    uint32_t padding_space = 0;

    // writing pixels line by line in order to add padding after each line
    for (size_t i = 0; i < img->height; i++) {

        size_t write_pixels = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        size_t write_padding = fwrite(&padding_space, sizeof(int8_t), padding, out);

        if (write_pixels != img->width || write_padding != padding) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
