#include <../include/deserializer.h>
#include <../include/error.h>
#include <../include/rotate.h>
#include <../include/serializer.h>


const char* error_messages[] = {
        "",
        "Memory error",
        "Invalid signature",
        "Invalid bits",
        "Invalid header"
};

int main(int argc, char** argv) {

    if (argc != 4) {
        fprintf(stderr, "Invalid arguments format\n");
        fprintf(stderr, "Correct order: <source-image> <transformed-image> <angle>\n");
        return INVALID_ARGUMENTS;
    }

    char* input_image_path = argv[1];
    char* output_image_path = argv[2];
    // char* to int
    int64_t angle = strtoll(argv[3], NULL, 10);

    FILE* input_image = fopen(input_image_path, "rb"); // rb - read binary
    FILE* output_image = fopen(output_image_path, "wb"); // wb - write binary

    if (!input_image) {
        fprintf(stderr, "Error opening the file\n");
        return FILE_OPENING_ERROR;
    }

    struct image img = {0};
    enum read_status read = from_bmp(input_image, &img);

    if (read != READ_OK) {
        fprintf(stderr, "%s\n", error_messages[read]);
        return IMAGE_DESERIALIZING_ERROR;
    }


    rotate(&img, angle);

    enum write_status write = to_bmp(output_image, &img);
    free_image(&img);

    switch (write) {

        case WRITE_OK:
            printf("Done!");
            break;

        case WRITE_ERROR:
            fprintf(stderr, "An error occurred during writing");
            return IMAGE_SERIALIZING_ERROR;
    }

    return 0;
}
